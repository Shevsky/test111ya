const url = 'https://api-maps.yandex.ru/2.1/';

function loadScript(url) {
	return new Promise((resolve, reject) => {
		const script = document.createElement('script');

		script.type = 'text/javascript';
		script.onload = resolve;
		script.onerror = reject;
		script.src = url;
		script.async = 'async';

		document.head.appendChild(script);
	});
}

export default class Index {
	constructor(ns = 'yandex-maps-loader', lang = 'ru_RU') {
		this.ns = ns;
		this.lang = lang;
		this.api = typeof window !== 'undefined' ? window[this.ns] || null : null;
		this.promise = null;
	}

	get onload() {
		return `${this.ns}__onload`;
	}

	get onerror() {
		return `${this.ns}__onerror`;
	}

	get loading() {
		return `${this.ns}__loading`;
	}

	get loaded() {
		return `${this.ns}__loaded`;
	}

	prepareQuery(query = {}) {
		const options = Object.assign({ ns: this.ns, lang: this.lang }, query, {
			onload: this.onload,
			onerror: this.onerror,
		});

		return Object.keys(options)
			.reduce((prev, key) => {
				if (typeof options[key] === 'undefined') return prev;
				return prev.concat(`${key}=${options[key]}`);
			}, [])
			.join('&');
	}

	load(query = {}) {
		if (this.promise) return this.promise;

		this.promise = new Promise((resolve, reject) => {
			query = this.prepareQuery(query);

			window[this.onload] = api => {
				resolve(api);
				window[this.onload] = null;
			};

			window[this.onerror] = error => {
				reject(error);
				window[this.onerror] = null;
			};

			loadScript(`${url}?${query}`).catch(reject);
		});

		return this.promise;
	}

	get(query = {}) {
		if (window[this.ns]) {
			this.api = window[this.ns];
			return Promise.resolve(this.api);
		}

		if (window[this.loading]) {
			return new Promise(resolve => {
				document.addEventListener(this.loaded, (e) => {
					const { detail: { api } } = e;

					this.api = api;

					resolve(api);
				});
			});
		}

		window[this.loading] = true;

		if (this.api) return Promise.resolve(this.api);

		return this.load(query).then(api => {
			this.api = api;

			const event = new CustomEvent(this.loaded, {
				detail: {
					api
				}
			});
			document.dispatchEvent(event);

			window[this.loading] = false;
			window[this.ns] = api;

			return api;
		});
	}
}